# Readme

- [Readme](#readme)
  - [Demo](#demo)
  - [How to](#how-to)
    - [Previously](#previously)
    - [How to run](#how-to-run)
      - [Tests](#tests)
  - [To Do](#to-do)
  - [Assumptions](#assumptions)
  - [Requirements](#requirements)
    - [dogs-challenge-pablohiyano](#dogs-challenge-pablohiyano)
      - [Foreword](#foreword)
    - [Challenge - Dog Team](#challenge---dog-team)
      - [What should you build?](#what-should-you-build)
    - [Notes](#notes)

## Demo

See the demo [here](https://doggy-fawn.vercel.app/)

## How to

### Previously

Have NodeJS installed. [NodeJS](https://nodejs.org/es/)

### How to run

```bash
npm i
npm start
```

#### Tests


```bash
npm start
```

and in another terminal

```bash
npm cy:open
```

## To Do
* Handler requests
* use fixture in tests

## Assumptions

Scrolling pages

## Requirements

### dogs-challenge-pablohiyano

#### Foreword

First, thanks for making the time for this challenge! We really appreciate it.

### Challenge - Dog Team

#### What should you build?

Design a small React app that shows a list of breeds using the api "Dog API" (https://dog.ceo/dog-api/). Add the ability to search the list by breed name. When a breed on the list is clicked, the app should navigate to the breed page.

The breed page: this page should show a list of dog pictures of the breed. This list should have a traditional pager or a scrolling pager, you decide. Add a button "Add to my team" to each dog.

When "add to my team" button is clicked, the dog should be added to the user's team. The user's team can have a maximum of 10 dogs and cannot have more than 3 dogs of the same breed.

My team section: this section should display the user's team. The dogs should be grouped by breed in the UI. Add a button to remove a dog from the user's team.

For saving the data of "My team section" use something local, may be cookies, local storage or something else. But don't use anything on the server side.

### Notes

- Add a README with everything you consider necessary to run the app
- Do not develop any backend code
- If you make any assumptions, take note of them in the README, so we can better understand your decisions.
- We are open for questions if you have any doubts
