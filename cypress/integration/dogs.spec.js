import random from "lodash/random";
/// <reference types="cypress" />

context("Dogs", () => {
  beforeEach(() => {
    cy.visit("/");
    // cy.fixture('all.json').as('all');
    cy.intercept("GET", "https://dog.ceo/api/breeds/list/all", {
      fixture: "all.json",
    }).as("all");
  });

  // const getSubBreed =

  it("Should add 2 dogs to my team", () => {
    let firstDog;
    let secondDog;

    cy.wait("@all").then((interception) => {
      const data = Object.entries(interception.response.body.message);

      const [breed1, subBreed1] = data[Math.floor(Math.random() * data.length)];
      firstDog =
        subBreed1.length > 0
          ? `${breed1}-${
              subBreed1[Math.floor(Math.random() * subBreed1.length)]
            }`
          : breed1;
      const [breed2, subBreed2] = data[Math.floor(Math.random() * data.length)];
      secondDog =
        subBreed2.length > 0
          ? `${breed2}-${
              subBreed2[Math.floor(Math.random() * subBreed2.length)]
            }`
          : breed2;

      cy.get('[data-test="home-nav-link"]').as("goToHome");
      cy.get('[data-test="my-team-nav-link"]').as("goToMyTeam");

      cy.get(`[data-test="name-dog-${firstDog}"]`).click();

      cy.get('[data-test="save-dog-button"]').as("saveDog");

      cy.location().should((loc) => {
        expect(loc.pathname).to.eq(`/detail/${firstDog}`);
      });

      cy.get("@saveDog").click();
      cy.get("@goToHome").click();

      cy.get(`[data-test="name-dog-${secondDog}"]`).click();

      cy.location().should((loc) => {
        expect(loc.pathname).to.eq(`/detail/${secondDog}`);
      });

      cy.get("@saveDog").click();
      cy.get("@goToMyTeam").click();

      cy.get(`[data-test="name-dog-${firstDog}"]`).should("exist");
      cy.get(`[data-test="name-dog-${secondDog}"]`).should("exist");
    });
  });

  it("Should add 2 dogs to my team and remove 1 dog from my team", () => {
    let firstDog;
    let secondDog;

    cy.wait("@all").then((interception) => {
      const data = Object.entries(interception.response.body.message);

      const [breed1, subBreed1] = data[Math.floor(Math.random() * data.length)];
      firstDog =
        subBreed1.length > 0
          ? `${breed1}-${
              subBreed1[Math.floor(Math.random() * subBreed1.length)]
            }`
          : breed1;
      const [breed2, subBreed2] = data[Math.floor(Math.random() * data.length)];
      secondDog =
        subBreed2.length > 0
          ? `${breed2}-${
              subBreed2[Math.floor(Math.random() * subBreed2.length)]
            }`
          : breed2;

      cy.log({ firstDog, secondDog });

      cy.get('[data-test="home-nav-link"]').as("goToHome");
      cy.get('[data-test="my-team-nav-link"]').as("goToMyTeam");

      cy.get(`[data-test="name-dog-${firstDog}"]`).click();

      cy.get('[data-test="save-dog-button"]').as("saveDog");

      cy.location().should((loc) => {
        expect(loc.pathname).to.eq(`/detail/${firstDog}`);
      });

      cy.get("@saveDog").click();
      cy.get("@goToHome").click();

      cy.get(`[data-test="name-dog-${secondDog}"]`).click();

      cy.location().should((loc) => {
        expect(loc.pathname).to.eq(`/detail/${secondDog}`);
      });

      cy.get("@saveDog").click();
      cy.get("@goToMyTeam").click();

      cy.get(`[data-test="name-dog-${firstDog}"]`).should("exist");
      cy.get(`[data-test="name-dog-${secondDog}"]`).should("exist");

      cy.get(`[data-test="name-dog-${firstDog}"]`).within(($dog) => {
        cy.get('[data-test="remove-dog-button"]').click();
      });
      cy.get(`[data-test="name-dog-${firstDog}"]`).should("not.exist");
      cy.get(`[data-test="name-dog-${secondDog}"]`).should("exist");
    });
  });
});
