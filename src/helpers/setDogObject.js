import uniqWith from "lodash/uniqWith";
import isEqual from "lodash/isEqual";
import {getDogs} from './localStorageHelper'

const setDogObject = (breed, subBreed, image) => {
  const currentDogs = getDogs();
  const subBreedObj = subBreed
    ? { id: `${breed}${subBreed}`, breed, subBreed, image }
    : { id: breed, breed, image };
  const updatedDogs = [...currentDogs, subBreedObj];
  return uniqWith(updatedDogs, isEqual);
};


export default setDogObject;
