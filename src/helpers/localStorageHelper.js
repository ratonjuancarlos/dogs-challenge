import ls from "local-storage";
import setDogObject from "./setDogObject";

export const getDogs = () =>
  !ls("dogs") ? ls("dogs", []) && ls("dogs") : ls("dogs");

export const checkLimitsBreed = (breed, value) => {
  const myDogs = getDogs();
  return myDogs.filter((dog) => dog.breed === breed).length < value;
};

export const checkLimitsDogs = (value) => {
  const myDogs = getDogs();
  return myDogs.length < value;
};

export const saveDog = (breed, subBreed, image) => {
  const updatedDogs = setDogObject(breed, subBreed, image);
  ls("dogs", updatedDogs);
  return true;
};

export const removeDog = (id) => {
  const myDogs = getDogs();
  const updatedDogs = myDogs.filter((dog) => dog.id !== id);
  ls("dogs", updatedDogs);
  return updatedDogs;
};

export const isInMyTeam = (breed, sb = "") => {
  const currentDogs = getDogs();
  const id = `${breed}${sb}`;
  const isInMyTeam = currentDogs.filter((dog) => dog.id === id).length > 0;
  return isInMyTeam;
};
