export const API_URL = "https://dog.ceo/api";
export const MAX_DOGS = 10;
export const MAX_PER_BREED = 3;

const constants = {
  API_URL,
  MAX_DOGS,
  MAX_PER_BREED,
};

export default constants;
