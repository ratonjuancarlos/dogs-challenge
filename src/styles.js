import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  '@global': {
    'a': {
      textDecoration: 'inherit',
      color: 'inherit'
    },
  },
  root: {
    flexGrow: 1,
  },
  title: {
    color: theme.palette.common.white,
  },
  body: {
    margin: 15,
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

export default useStyles;
