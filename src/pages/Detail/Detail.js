import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import useFetch from "use-http";
import Gallery from "components/Gallery";
import HeaderDetail from "components/HeaderDetail";
import Actions from "components/Actions";
import { API_URL } from "constants.js";

const Detail = () => {
  const { get, response, loading, error } = useFetch(`${API_URL}/breed`);
  const { dog } = useParams();
  const [breed, subBreed] = dog.split("-");
  const url = subBreed ? `${breed}/${subBreed}` : `${breed}`;
  const [images, setImages] = useState([]);

  useEffect(() => {
    async function loadInitialImages() {
      const initialImages = await get(`/${url}/images`);
      if (response.ok) setImages(initialImages.message);
    }
    loadInitialImages();
  }, [setImages, get, response, url]);

  return (
    <>
      {error && "Error!"}
      {loading && "Loading..."}
      {images.length > 0 && (
        <>
          <HeaderDetail {...{ breed, subBreed }} />
          <Gallery {...{ images, breed, subBreed, dog }} />
          <Actions {...{ breed, subBreed, image: images[0] }} />
        </>
      )}
    </>
  );
};

export default Detail;
