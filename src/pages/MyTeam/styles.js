import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  breed: {
    marginBottom: 50,
  },
  subBreed: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
}));

export default useStyles;
