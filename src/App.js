import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "pages/Home";
import MyTeam from "pages/MyTeam";
import Detail from "pages/Detail";
import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import useStyles from "./styles";
import CssBaseline from '@material-ui/core/CssBaseline';

export default function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Router>
        <AppBar position="static">
          <Toolbar className={classes.toolbar}>
            <Link to="/" data-test="home-nav-link">
              <Typography variant="h6" className={classes.title}>
                Home
              </Typography>
            </Link>
            <Link to="/my-team" data-test="my-team-nav-link">
              <Typography variant="h6" className={classes.title}>
                My Team
              </Typography>
            </Link>
          </Toolbar>
        </AppBar>
        <div className={classes.body}>
          <Switch>
            <Route path="/detail/:dog">
              <Detail />
            </Route>
            <Route path="/my-team">
              <MyTeam />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}
