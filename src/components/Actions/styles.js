import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  actions: {
    margin: theme.spacing(2, 0),
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  googleLink: {
    margin: "15px 0",
  },
}));

export default useStyles;
