import React  from "react";
import PropTypes from "prop-types";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import ImageDialog from 'components/ImageDialog'
import useStyles from './styles'

const Gallery = ({ images, breed, subBreed }) => {
  const [open, setOpen] = React.useState(false);
  const [selectedImage, setSelectedImage] = React.useState();
  const classes = useStyles();
  const handleClickOpen = (image) => {
    setSelectedImage(image);
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <GridList className={classes.gridList} cellHeight={600} cols={2.5}>
        {images.map((image) => (
          <GridListTile key={image}>
            <img
              src={image}
              alt={breed}
              onClick={() => handleClickOpen(image)}
            />
            <GridListTileBar
              title={`${breed} ${subBreed ? subBreed : ""} - Click to open it`}
              classes={{
                root: classes.titleBar,
                title: classes.title,
              }}
            />
          </GridListTile>
        ))}
      </GridList>
      <ImageDialog
        selectedImage={selectedImage}
        open={open}
        breed={breed}
        onClose={handleClose}
      />
    </div>
  );
};

Gallery.propTypes = {
  images: PropTypes.array,
  breed: PropTypes.string,
  subBreed: PropTypes.string,
};

export default Gallery;
